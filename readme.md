# My Kubernetes Journey

My 2024 New Year's resolution is to become a [Kubernetes contributor](https://github.com/kubernetes/community/blob/master/contributors/guide/README.md).

When I started learning "the Internet" 20 years ago, CSS wasn't a thing, and PHP 3 had just been released. I've made a lot of progress since then, but I missed the Kubernetes train.

Time to go back to the night lab! 🌘 🖥

## Train

### 2024-05-13: [CKA obtained](https://ti-user-certificates.s3.amazonaws.com/e0df7fbf-a057-42af-8a1f-590912be5460/2b01f0e0-397b-49b8-a81e-1f0628e10548-frdric-potvin-e4366b32-2f13-452a-accb-adf0ece8782f-certificate.pdf)

After 11 weeks of intense studying, I finally passed the CKA exam with a score of 86 out of 100!

Fun story: during the exam, my laptop started experiencing kernel crashes and became unresponsive. I forgot to plug in the power adapter! Because the PSI browser was full screen and blocked all notifications, I didn't see the alert beforehand. I had to reauthenticate to the exam, losing 30 minutes. Given that extra time constraint, I couldn't finish the etcd recovery and network policy questions properly.

### 2024-05-09: [Killer.sh CKA Simulator](https://killer.sh/cka)

Completed it brilliantly with a score of 119/125 in 5.5 hours. I had to divide it into 3 sessions to get through all the questions. I was way too slow for the actual exam, but at least my score shows my thorough understanding of the foundational knowledge.

### 2024-04-23: [LFS258 - Kubernetes Fundamentals](https://training.linuxfoundation.org/training/kubernetes-fundamentals/)

Completed: [Certificate](https://ti-user-certificates.s3.amazonaws.com/e0df7fbf-a057-42af-8a1f-590912be5460/2b01f0e0-397b-49b8-a81e-1f0628e10548-frederic-potvin-71e80add-19bb-489d-bf43-bf83a5e9be5e-certificate.pdf).

### 2024-03-18: Up and Running!

Completed study and labs from [Kubernetes: Up and Running](https://www.oreilly.com/library/view/kubernetes-up-and/9781491935668/).

### 2024-02-28: LFS258 + CKA Bundle

Having goals and deadlines is important. As such, I purchased the [LFS258 + CKA bundle](https://training.linuxfoundation.org/training/kubernetes-fundamentals-lfs258-cka-exam-bundle/). The clock is ticking! 365 days left to get the cert :)

### 2024-02 to 04: [Home Lab](//gitlab.com/gnufred/myk8s/-/blob/main/lab.md)

Lab experiments located in `/vagrant`. I use Vagrant and Ansible to deploy: an HAProxy load balancer, 3 highly available control plane nodes, an NFS server, and 6 worker nodes.

Learning the hard way, I guess :)

### 2024-02: Courses, and LFCS certification.

After a year of relearning Go, it was time to dive into sysadmin stuff.

I completed the following courses and earned these certificates:

* [LFS207: Linux System Administration Essentials](https://ti-user-certificates.s3.amazonaws.com/e0df7fbf-a057-42af-8a1f-590912be5460/2b01f0e0-397b-49b8-a81e-1f0628e10548-frdric-potvin-f216780b-5861-4a99-b8bd-68de9b89f4b9-certificate.pdf)
* [LFS151.x: Introduction to Cloud Infrastructure Technologies](https://courses.edx.org/certificates/b51f3461a2124e77a3916126d5436e47)
* [Linux Foundation Certified System Administrator (LFCS)](https://ti-user-certificates.s3.amazonaws.com/e0df7fbf-a057-42af-8a1f-590912be5460/2b01f0e0-397b-49b8-a81e-1f0628e10548-frederic-potvin-5a5f270c-2b75-45e6-b648-f28a23c01577-certificate.pdf)

### 2023-04 to 2024-01: [Logslate](https://gitlab.com/gnufred/logslate/)

This was an almost year-long project, which was promoted to an official security tool used at [Platform.sh](https://platform.sh). I spent many weekends and holidays coding and studying security.

### 2023-03: [Learning Go](https://www.oreilly.com/library/view/learning-go/9781492077206/)

This was the stepping stone. At the time, I didn't know that my drive to become a Go master would lead me to discover the CNCF ecosystem and Kubernetes itself. It's entirely Google's fault since they created both Go and Kubernetes, which everyone started using to re-engineer the whole Internet.
