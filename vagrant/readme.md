# MyK8s Vagrant lab

Latest Vagrant configuration for my K8s labs.

## Usage

### Provision

Use `run/provision`.

### Playbook

Use the Playbook helper to run playbooks, roles, or tags on the Vagrant VM. 

Read `run/playbook --help`.

### Kubectl

Use `run/kubectl` to run Kubectl commands on the Vagrant VM.
